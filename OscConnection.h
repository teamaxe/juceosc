/*
  ==============================================================================

    OscConnection.h
    Created: 8 Sep 2012 12:04:20pm
    Author:  tjmitche

  ==============================================================================
*/

#ifndef H_OSCCONNECTION
#define H_OSCCONNECTION

#include "../JuceLibraryCode/JuceHeader.h"
#include "OscMessage.h"

class OscConnection : public Thread
{
public:
    /** Constructor */
    OscConnection();
    
    /** Constructor */
    OscConnection (String sendHostname, uint16 sendPort);
    
    /** Destructor */
    ~OscConnection();
    
    /** Binds to the specified port number and listens for incoming messages.
     Inherit from OscListener to recieve them. */
    void listen (uint16 portNumber);
    
    /** stops listening on the specified port */
    void stopListening();
    
    /**  checks to see if we are connected to a remote host */
    bool isConnected() const;
    
    //Thread
    virtual void run();
    
    /** Base class for recieving OSC messages  */
    class Listener
    {
    public:
        /** Destructor */
        virtual ~Listener() {}
        
        /** Called when a connection has been made see */
        virtual void oscMessageRecieved (OscMessage* message) = 0;
        
        /** If you want error messages override this */
        virtual void oscConnectionError (String errorMessage) { DBG (errorMessage); }
    };
    
    /** function to add listeners to this connection */
    void addListener(OscConnection::Listener *listenerToAdd);
    
    /** function to remove listeners from this connection */
    void removeListener(OscConnection::Listener *listenerToRemove);
    
    /** Writes the provided OscMessage to the output socket */
    bool write (OscMessage& message);
    
    /** Writes the provided OscMessage to the output socket */
    bool write (OscMessage& message, String ipToSendTo, int portToSendTo);
    
    /** gets the current local port number*/
    uint16 getListenPortNumber() const
    {
        return listenPort;
    }
    
    /** gets the current send port number*/
    uint16 getSendPort() const
    {
        return sendPort;
    }
    
    /** sets the current send port number */
    void setSendPort (uint16 newPortNumber)
    {
        sendPort = newPortNumber;
    }
    
    /** gets the current send port number */
    const String& getSendHostname() const
    {
        return sendHostname;
    }
    
    /** sets the current send port number */
    void setSendHostname (const String& newHostname)
    {
        sendHostname = newHostname;
    }
    
private:
    uint16 listenPort;  //port number for incoming messages
    uint16 sendPort;    //port number for outgoing messages
    String sendHostname;//name for remote host
    
    ScopedPointer<DatagramSocket> sendSocket;
    ScopedPointer<DatagramSocket> receiveSocket;
    
    CriticalSection listenerLock;
    ListenerList<Listener> listeners;
};

#endif  // H_OSCCONNECTION
