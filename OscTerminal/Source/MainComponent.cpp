/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"

//==============================================================================
MainContentComponent::MainContentComponent()
{
    appData = oscSettings.getValueTree();
    
    sendIpLabel.getTextValue().referTo (appData.getPropertyAsValue (OscSettings::Ids::SendIP, nullptr));
    sendPortLabel.getTextValue().referTo (appData.getPropertyAsValue (OscSettings::Ids::SendPort, nullptr));
    sendTextLabel.getTextValue().referTo (appData.getPropertyAsValue (OscSettings::Ids::SendText, nullptr));
    listenPortLabel.getTextValue().referTo (appData.getPropertyAsValue (OscSettings::Ids::ListenPort, nullptr));

    osc.addListener(this);
    
    sendIpLabel.setEditable(true);
    addAndMakeVisible(&sendIpLabel);
    
    sendPortLabel.setEditable(true);
    addAndMakeVisible(&sendPortLabel);
    
    connectButton.setButtonText("connect");
    connectButton.addListener(this);
    addAndMakeVisible(&connectButton);
    
    sendTextLabel.setEditable(true);
    addAndMakeVisible(&sendTextLabel);
    
    sendButton.setButtonText("send");
    sendButton.addListener(this);
    addAndMakeVisible(&sendButton);
    
    listenPortLabel.setEditable(true);
    addAndMakeVisible (&listenPortLabel);
    
    listenButton.setButtonText("listen");
    addAndMakeVisible (&listenButton);
    
    consoleOutput.setMultiLine(true);
    addAndMakeVisible(&consoleOutput);
    
    osc.setSendPort (sendPortLabel.getText().getIntValue());
    osc.setSendHostname (sendIpLabel.getText());
    
    setSize (500, 400);
}

MainContentComponent::~MainContentComponent()
{
    osc.removeListener(this);
}

void MainContentComponent::paint (Graphics& g)
{
    g.fillAll (Colour (0xffeeddff));

    g.setFont (Font (16.0f));
    g.setColour (Colours::black);
    g.drawText ("Hello World!", getLocalBounds(), Justification::centred, true);
}

void MainContentComponent::resized()
{
    int xUnit = getWidth() / 3;
    int yUnit = 20;
    sendIpLabel.setBounds (0, 0, xUnit, yUnit);
    sendPortLabel.setBounds (sendIpLabel.getBounds().translated (xUnit, 0));
    connectButton.setBounds (sendPortLabel.getBounds().translated (xUnit, 0));
    sendTextLabel.setBounds (0, sendIpLabel.getBottom(), 2 * xUnit, yUnit);
    sendButton.setBounds (connectButton.getBounds().translated (0, yUnit));
    listenPortLabel.setBounds(sendPortLabel.getBounds().translated (0,2*yUnit));
    listenButton.setBounds(listenPortLabel.getBounds().translated (xUnit, 0));
    
    consoleOutput.setBounds(0, listenPortLabel.getBottom(), getWidth(), getHeight()-listenPortLabel.getBottom());
}
