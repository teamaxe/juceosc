//
//  OscSettings.h
//  OscTerminal
//
//  Created by tj3-mitchell on 28/11/2013.
//
//

#ifndef __OscTerminal__OscSettings__
#define __OscTerminal__OscSettings__

#include "../JuceLibraryCode/JuceHeader.h"

class OscSettings
{
public:
	OscSettings();
	
	~OscSettings();
	
	ValueTree getValueTree()	{	return settingsTree;	}
    
	//==============================================================================
    /** a struct containing Identifier variables for the OSC settings */
    struct Ids
    {
        static const Identifier OSCSETTINGS;                /**< the OSC Settings identifier */
        static const Identifier SendIP;                     /**< the number of scenes in the arrangement */
        static const Identifier SendPort;                   /**< the index of the current active scene */
        static const Identifier SendText;
        static const Identifier ListenPort;
    };
    
private:
	
	bool loadSettingsFile(File settingsFile, ValueTree &treeToFill);
	bool buildDefaultSettings(ValueTree &treeToFill);
	bool saveSettingsFile(File settingsFile, ValueTree &treeToSave);
	
	ValueTree settingsTree;
};



#endif /* defined(__OscTerminal__OscSettings__) */
