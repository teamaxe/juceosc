/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "OscSettings.h"
#include "../../OscConnection.h"

//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainContentComponent   :  public Component,
                                public Button::Listener,
                                public OscConnection::Listener,
                                public MessageListener,
                                public Timer
{
public:
    //==============================================================================
    MainContentComponent();
    ~MainContentComponent();

    void paint (Graphics&);
    void resized();

    void buttonClicked (Button* button)
    {
        if (button == &connectButton)
        {
            if (osc.isConnected())
            {
                osc.stopListening();
            }
            else
            {
                osc.listen (listenPortLabel.getText().getIntValue());
            }
            startTimer (100);
        }
        else if (button == &sendButton)
        {
            OscMessage msg;
            msg.addAddress ("/serialosc/list");
            msg.addStringArgument ("127.0.0.1");
            msg.addInt32Argument (12002);
            osc.write (msg);
            
            std::cout << msg.asString() << "\n";

        }
    }
    
    class TextMessage : public Message
    {
    public:
        TextMessage (String initialText) : text (initialText) {}
        String text;
    };
    
    void oscMessageRecieved (OscMessage* message)
    {
        if (message->getArgumentType (0) == OscMessage::BlobType)
        {
            std::cout << "Size" << message->getBlobSize(0) << ":";
            const char* blob = message->getArgumentAsBlob (0);
            for (int i = 0; i < message->getBlobSize(0); i++)
            {
                std::cout << blob[i] << " ";
            }
            std::cout << "\n";
        }
        String messageString (message->asString());
        //std::cout << message->asString() << "\n";
        postMessage(new TextMessage (messageString));
    }
    
    void handleMessage (const Message& message)
    {
        const TextMessage& mes = dynamic_cast<const TextMessage&> (message);
        consoleOutput.insertTextAtCaret(mes.text + "\n");
    }
    
    void timerCallback()
    {
        stopTimer();
        connectButton.setToggleState (osc.isConnected(), dontSendNotification);
    }


private:
    Label sendIpLabel;
    Label sendPortLabel;
    TextButton connectButton;
    Label sendTextLabel;
    TextButton sendButton;
    
    Label listenPortLabel;
    TextButton listenButton;
    
    TextEditor consoleOutput;
    
    OscSettings oscSettings;
    
    ValueTree appData;
    
    OscConnection osc;
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainContentComponent)
};


#endif  // MAINCOMPONENT_H_INCLUDED
