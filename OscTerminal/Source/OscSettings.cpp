//
//  OscSettings.cpp
//  OscTerminal
//
//  Created by tj3-mitchell on 28/11/2013.
//
//

#include "OscSettings.h"

const Identifier OscSettings::Ids::OSCSETTINGS("OSCSETTINGS");
const Identifier OscSettings::Ids::SendIP("SendIP");
const Identifier OscSettings::Ids::SendPort("SendPort");
const Identifier OscSettings::Ids::SendText("SendText");
const Identifier OscSettings::Ids::ListenPort("ListenPort");


OscSettings::OscSettings()
{
	// load previous settings if possible
    File resourcesFolder(File::getSpecialLocation(File::currentExecutableFile).getParentDirectory().getParentDirectory().getChildFile("Resources"));
    resourcesFolder.createDirectory();
    File settingsFile(resourcesFolder.getChildFile("settings.xml"));
    if (loadSettingsFile(settingsFile, settingsTree) == false)
    {
        buildDefaultSettings(settingsTree);
    }
}

OscSettings::~OscSettings()
{
	File resourcesFolder(File::getSpecialLocation(File::currentExecutableFile).getParentDirectory().getParentDirectory().getChildFile("Resources"));
	File settingsFile(resourcesFolder.getChildFile("settings.xml"));
	saveSettingsFile(settingsFile, settingsTree);
	
	ScopedPointer<XmlElement> xml(settingsTree.createXml());
	if (&xml != nullptr) {
		DBG(xml->createDocument(""));
	}
}

bool OscSettings::loadSettingsFile(File settingsFile, ValueTree &treeToFill)
{
	if (settingsFile.existsAsFile())
	{
		XmlDocument settingsDoc (settingsFile);
		ScopedPointer<XmlElement> settingsXML(settingsDoc.getDocumentElement());
		
		treeToFill = ValueTree::fromXml(*settingsXML);
		
		return treeToFill.isValid();
	}
	
	return false;
}

bool OscSettings::buildDefaultSettings(ValueTree &treeToFill)
{
	treeToFill = ValueTree(Ids::OSCSETTINGS);
	settingsTree.setProperty(Ids::SendIP, "127.0.0.1", nullptr);
	settingsTree.setProperty(Ids::SendPort, 9000, nullptr);
    settingsTree.setProperty(Ids::SendText, "send text...", nullptr);
    settingsTree.setProperty(Ids::ListenPort, 8000, nullptr);
    
	return treeToFill.isValid();
}

bool OscSettings::saveSettingsFile(File settingsFile, ValueTree &treeToSave)
{
	if (! settingsFile.existsAsFile()) {
		settingsFile.create();
	}
	
	if (treeToSave.isValid())
	{
		ScopedPointer<XmlElement> settingsXml(treeToSave.createXml());
		if (settingsXml->writeToFile(settingsFile, "")) {
			DBG("XML File written");
			return true;
		}
		else {
			DBG("Error in saving settings");
			return false;
		}
	}
	else
		DBG("Settings not valid to save");
	
	return false;
}
