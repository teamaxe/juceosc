/*
 ==============================================================================
 
 OscConnection.cpp
 Created: 8 Sep 2012 12:04:20pm
 Author:  tjmitche
 
 ==============================================================================
 */
/**
 Class that manages an OscConnection using a UDP socket.
 */
#include "OscConnection.h"

OscConnection::OscConnection() :   Thread("OscConnection")
{
    sendSocket = new DatagramSocket (true);
}

OscConnection::OscConnection (String sendHostnameToUse, uint16 sendPortToUse) :   Thread("OscConnection")
{
    setSendHostname (sendHostnameToUse);
    setSendPort (sendPortToUse);
    sendSocket = new DatagramSocket (true);
    receiveSocket = new DatagramSocket();
}

OscConnection::~OscConnection()    
{
    stopListening();
    sendSocket = nullptr;
}

void OscConnection::listen (uint16 portNumber)
{
    stopListening();
    
    listenPort = portNumber;
    startThread (5);
}

void OscConnection::stopListening()
{
    if (isThreadRunning())
    {
        signalThreadShouldExit();
        receiveSocket = nullptr;
        stopThread (-1);
    }
}

bool OscConnection::isConnected() const
{
    return receiveSocket != nullptr && isThreadRunning();
}

//Thread
void OscConnection::run()
{
    const int size = 65535; //max theoretical message size
    char buffer[65535];
    
    String senderIp;
    int senderPort;
    
    receiveSocket = new DatagramSocket();
    
    if (receiveSocket->bindToPort (listenPort) == false) //error
    {
        ScopedLock sl (listenerLock);
        listeners.call (&Listener::oscConnectionError, String("Error binding to port: ") + String (listenPort));
        return;
    }
    else
    {
        DBG ("Binding to port: " << listenPort);
    }
    
    while (! threadShouldExit())
    {
        const int ready = receiveSocket->waitUntilReady (true, -1);
        
        if (ready == 1) //success
        {
            int bytesRead = receiveSocket->read (buffer, size, false, senderIp, senderPort);
            if (bytesRead < 0)
            {
                DBG ("error with socket::read\n");
                break;
            }
            else
            {
//                for (int i = 0; i < bytesRead; i++)
//                {
//                    printf("%c", *(buffer+i));
//                }
//                printf("\n");
                
                //parseOscMessage(cptr, bytesRead);
                OscMessage message (buffer, bytesRead);
                
                //                    for (int i = 0; i < message.getNumerOfArguments(); i++)
                //                    {
                //                        if (message.getArgumentType(i) == OscMessage::IntType)
                //                        {
                //                            DBG(String(i) + String("\t:") + String(message.getArgumentAsInt(i)));
                //                        }
                //                        else if (message.getArgumentType(i) == OscMessage::FloatType)
                //                        {
                //                            DBG(String(i) + String("\t:") + String(message.getArgumentAsFloat(i)));
                //                        }
                //                    }
                ScopedLock sl(listenerLock);
                listeners.call(&Listener::oscMessageRecieved, &message);
            }
        }
        else
        {
            ScopedLock sl (listenerLock);
            listeners.call (&Listener::oscConnectionError, String("Listen socket error on port: ") + String (listenPort));
            break;
        }
    }
    DBG ("Releaseing port: " << listenPort);
}

void OscConnection::addListener(OscConnection::Listener *listenerToAdd)
{
    if (listenerToAdd != nullptr) 
    {
        ScopedLock sl(listenerLock);
        listeners.add(listenerToAdd);
    }
}

void OscConnection::removeListener(OscConnection::Listener *listenerToRemove)
{
    if (listenerToRemove != nullptr) 
    {
        ScopedLock sl(listenerLock);
        listeners.remove(listenerToRemove);
    }
}

bool OscConnection::write (OscMessage& message)
{
    if (sendSocket == nullptr)
        return false;
    
    int dataSize = message.getRawDataSize();
//    const char* const data = message.getRawData();
//    
//    for (int i = 0; i < dataSize; i++)
//    {
//        printf("%2d | %c | %10x\n", i, data[i], data[i]);
//    }
    
    
    int numWritten = sendSocket->write (sendHostname, sendPort, message.getRawData(), dataSize);
    
//    DBG(sendHostname << sendPort);
    if (numWritten == dataSize)
        return true;
    else 
        return false;
}

bool OscConnection::write (OscMessage& message, String ipToSendTo, int portToSendTo)
{
    if (sendSocket == nullptr)
        return false;
    
    int dataSize = message.getRawDataSize();
    
    int numWritten = sendSocket->write (ipToSendTo, portToSendTo, message.getRawData(), dataSize);
    
    //    DBG(sendHostname << sendPort);
    if (numWritten == dataSize)
        return true;
    else
        return false;
}