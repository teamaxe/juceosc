/*
  ==============================================================================

    OscConnectionTcp.h
    Created: 8 Sep 2012 12:04:20pm
    Author:  tjmitche

  ==============================================================================
*/

#ifndef H_OSCCONNECTIONTCP
#define H_OSCCONNECTIONTCP

#include "../JuceLibraryCode/JuceHeader.h"
#include "OscMessage.h"

class OscConnectionTcp : public Thread
{
public:
    /**
     Constructor
     */
    OscConnectionTcp ( const uint16 localPortNumber_ = 0, 
                    const uint16 remotePortNumber_ = 0, 
                    const String & remoteHostname_ = "localhost" );
    
    /** Destructor */
    ~OscConnectionTcp();
    
    /** Disconects the OSC connection */
    void disconnect();
    
    /** Binds to the local port number @ localPortNumber and listens for incoming messages. 
     Inherit from OscListener to recieve them. */
    void listen();
    
    /** Binds to the specified local port and listens for incoming messages. 
     Inherit from OscListener to recieve them. */
    void listenOnPort (const int localPortNumber_);
    
    /** returns the connection state */
    bool isConnected();
    
    //Thread
    virtual void run();
    
    /** Base class for recieving OSC messages  */
    class Listener
    {
    public:
        /** Destructor */
        virtual ~Listener() {}
        
        /** Enum to differentiate local/remote connection - args to the MadeLost functions? */
        enum ConnectionType
        {
            Local = 1,
            Remote
        };
        
        /** Called when a connection has been made see */
        virtual void oscConnectionMade (ConnectionType madeConnectionType) {}
        virtual void oscConnectionLost (ConnectionType lostConnectionType) {}
        virtual void oscMessageRecieved (OscMessage* message) = 0;
    };
    
    /** function to add listeners to this connection */
    void addListener(OscConnectionTcp::Listener *listenerToAdd);
    
    /** function to remove listeners from this connection */
    void removeListener(OscConnectionTcp::Listener *listenerToRemove);
    
    /** Connects to remote host with specified ip and port ready for sending osc messages. */
    bool connectToRemoteHost (const String & remoteHostname, const int remotePortNumber);
    
    /** Writes the provided OscMessage to the output socket */
    bool write (OscMessage& message);
    
private:
    uint16 localPortNumber;     //port number for incoming messages
    uint16 remotePortNumber;    //port number for outgoing messages
    String remoteHostname;      //name for remote host
    
    ScopedPointer<StreamingSocket> remoteTcpSocket;
    
    CriticalSection listenerLock;
    ListenerList<Listener> listeners;
};

#endif  // H_OSCCONNECTIONTCP
