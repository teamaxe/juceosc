/*
 ==============================================================================
 
 OscConnectionTcp.cpp
 Created: 8 Sep 2012 12:04:20pm
 Author:  tjmitche
 
 ==============================================================================
 */
/**
 Class that manages an OscConnectionTcp using a UDP socket.
 */

#include "OscConnectionTcp.h"

OscConnectionTcp::OscConnectionTcp (    const uint16 localPortNumber_,
                                        const uint16 remotePortNumber_, 
                                        const String & remoteHostname_ ) 
:   Thread("OscConnectionTcp"),
    localPortNumber(localPortNumber_),
    remotePortNumber(remotePortNumber_),
    remoteHostname(remoteHostname_)
{
    
}

OscConnectionTcp::~OscConnectionTcp()    
{
    disconnect();
}

void OscConnectionTcp::disconnect()
{
    if (remoteTcpSocket != nullptr && remoteTcpSocket->isConnected())
    {
        remoteTcpSocket->close();
    }
    if (isThreadRunning()) 
    {
        stopThread (500);
    }
    //connectionLostInternal();
}

void OscConnectionTcp::listen()
{
    if (isThreadRunning()) 
    {
        stopThread (500);
    }
    
    jassert (localPortNumber != 0);
    if (localPortNumber != 0) 
    {
        startThread(5);
        //DBG ("Listen thread running\n");
    }
    else 
    {
        DBG ("error: localport is 0 you need to set the local port number before listening -> try listenOnPort()\n");
    }
}

void OscConnectionTcp::listenOnPort (const int localPortNumber_)
{
    localPortNumber = localPortNumber_;
    this->listen();
}

bool OscConnectionTcp::isConnected()
{
    return remoteTcpSocket == nullptr ? false : remoteTcpSocket->isConnected();
}

//Thread
void OscConnectionTcp::run()
{
    DBG("TCP:Listen thread running, binding to port:" << localPortNumber << "\n");
    const int maxDatagramSize = 65535; //max theoretical message size
    ScopedPointer <DatagramSocket> socket (new DatagramSocket (localPortNumber));
    ScopedPointer <MemoryBlock> messageData (new MemoryBlock (maxDatagramSize, true));
    socket->bindToPort (localPortNumber-1);
    
    //char oscDataMessage[] = {0x2f, 0x78, 0x6f, 0x73, 0x63, 0x2f, 0x69, 0x6e, 0x0, 0x0, 0x0, 0x0, 0x2c, 0x69, 0x0, 0x0, 0x11, 0x11, 0x11, 0x11};
    
    while (! threadShouldExit())
    {
        if (socket != nullptr)
        {
            const int ready = socket->waitUntilReady (true, 200);//wait upto 200ms for messages to arrive
            
            if (ready < 0)
            {
                DBG ("Error waiting for socket\n");
                break;
            }
            else if (ready == 0)
            {
                //DBG ("Timeout waiting for socket\n");
            }
            else //read away
            {
                int bytesRead = socket->read(static_cast <char*> (messageData->getData()), maxDatagramSize, false );  
                if (bytesRead < 0)
                {
                    DBG ("error with socket::read\n");
                    break;
                }
                else 
                {
                    //                        for (int i = 0; i < bytesRead; i++) 
                    //                        {
                    //                            char* cptr = static_cast <char*> (messageData->getData());
                    //                            printf("%c", *(cptr+i));
                    //                        }
                    //                        
                    //                        printf("\n");
                    
//                    printf("\n**READStart**\n");
//                    for (int i = 0; i < bytesRead; i++) 
//                    {
//                        char* cptr = static_cast <char*> (messageData->getData());
//                        printf("%2d| %10x | %10x | %c\n", i, oscDataMessage[i], *(cptr+i), *(cptr+i));
//                        
//                    }
//                    printf("\n**READEnd**\n");
                    
                    const char* const cptr = static_cast <const char* const> (messageData->getData());
                    //parseOscMessage(cptr, bytesRead);
                    OscMessage message(cptr, bytesRead);
                    
//                    for (int i = 0; i < message.getNumerOfArguments(); i++) 
//                    {
//                        if (message.getArgumentType(i) == OscMessage::IntType) 
//                        {
//                            DBG(String(i) + String("\t:") + String(message.getArgumentAsInt(i)));
//                        }
//                        else if (message.getArgumentType(i) == OscMessage::FloatType) 
//                        {
//                            DBG(String(i) + String("\t:") + String(message.getArgumentAsFloat(i)));
//                        }
//                        
//                    }
                    ScopedLock sl(listenerLock);
                    listeners.call(&Listener::oscMessageRecieved, &message);
                }
            }
        }
        else
        {
            break;
        }
    }
    //disconnect the socket
    socket = nullptr;
    //connectionLostInt();
}

void OscConnectionTcp::addListener(OscConnectionTcp::Listener *listenerToAdd)
{
    if (listenerToAdd != nullptr) 
    {
        ScopedLock sl(listenerLock);
        listeners.add(listenerToAdd);
    }
}

void OscConnectionTcp::removeListener(OscConnectionTcp::Listener *listenerToRemove)
{
    if (listenerToRemove != nullptr) 
    {
        ScopedLock sl(listenerLock);
        listeners.remove(listenerToRemove);
    }
}

bool OscConnectionTcp::connectToRemoteHost (const String& remoteHostname, const int remotePortNumber)
{
    bool returnVal;
    remoteTcpSocket = new StreamingSocket();
    
    returnVal = remoteTcpSocket->connect (remoteHostname, remotePortNumber);
    
    if (returnVal) 
    {
        DBG ("osc remote connected!\n");
    }
    else 
    {
        std::cout << "notConnected!\n";
    }
    
    return returnVal;	
}

bool OscConnectionTcp::write (OscMessage& message)
{
    if (remoteTcpSocket == nullptr || remoteTcpSocket->isConnected() == false)
        return false;
    
    int dataSize = message.getRawDataSize();
//    const char* const data = message.getRawData();
//    
//    for (int i = 0; i < dataSize; i++)
//    {
//        printf("%2d | %c | %10x\n", i, data[i], data[i]);
//    }
    
    int numWritten = remoteTcpSocket->write(message.getRawData(), dataSize);
    
    if (numWritten == dataSize) 
        return true;
    else 
        return false;
}
